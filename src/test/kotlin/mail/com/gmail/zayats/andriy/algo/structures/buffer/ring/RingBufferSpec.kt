package mail.com.gmail.zayats.andriy.algo.structures.buffer.ring

import org.spekframework.spek2.Spek
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertFailsWith

object RingBufferSpec : Spek({
    
    group("initial state") {
        
        val capacity = 10
        val buffer = RingBuffer<Int>(capacity)
        
        test("capacity should be correct") {
            assertEquals(capacity, buffer.capacity)
        }

        test("offset should be 0") {
            assertEquals(0, buffer.offset)
        }

        test("size should be 0") {
            assertEquals(0, buffer.size)
        }
    }
    
    group("set/get") {
        
        test("get by index returns previously set value even after index is more than capacity") {
            val capacity = 5
            val buffer = RingBuffer<Int>(capacity)
            
            repeat(capacity * 3) { i ->
                val value = i * 7
                buffer[i] = value
                assertEquals(value, buffer[i])
            }
        }

        test("value change is possible till buffer is not over") {
            val capacity = 5
            val buffer = RingBuffer<Int>(capacity)

            repeat(capacity) { i ->
                val value = i * 7
                buffer[i] = value
            }
            
            val index = capacity - 2
            val newValue = 13
            buffer[index] = newValue
            assertEquals(newValue, buffer[index])
        }
    }
    
    group("properties") {

        test("size is increased after new index set") {
            val capacity = 5
            val buffer = RingBuffer<Int>(capacity)

            repeat(capacity * 3) { i ->
                val value = i * 7
                buffer[i] = value
                assertEquals(i + 1, buffer.size)
            }
        }

        test("offset is zero till capacity is enough and is increased after") {
            val capacity = 5
            val buffer = RingBuffer<Int>(capacity)

            repeat(capacity) { i ->
                val value = i * 7
                buffer[i] = value
                assertEquals(0, buffer.offset)
            }

            repeat(10) { i ->
                val value = i * 7
                buffer[i + capacity] = value
                assertEquals(i + 1, buffer.offset)
            }
        }
    }
    
    group("limitations") {
        
        test("indexes should be populated sequentially. Exception otherwise") {
            val capacity = 5
            val buffer = RingBuffer<Int>(capacity)
            buffer[0] = 0
            assertFails {
                buffer[2] = 2
            }
        }

        test("only previously populated indexes are available for retrieving. Exception otherwise.") {
            val capacity = 5
            val buffer = RingBuffer<Int>(capacity)
            buffer[0] = 0
            assertFails {
                buffer[1]
            }
        }

        test("cannot get index after buffer is over") {
            val capacity = 2
            val buffer = RingBuffer<Int>(capacity)
            buffer[0] = 0
            buffer[1] = 0
            buffer[2] = 0
            assertFails {
                buffer[0]
            }
        }

        test("cannot set (change) index value after buffer is over") {
            val capacity = 2
            val buffer = RingBuffer<Int>(capacity)
            buffer[0] = 0
            buffer[1] = 0
            buffer[2] = 0
            assertFails {
                buffer[0] = 1
            }
        }
    }
})
