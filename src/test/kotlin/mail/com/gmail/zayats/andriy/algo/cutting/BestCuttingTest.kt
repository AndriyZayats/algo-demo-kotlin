package mail.com.gmail.zayats.andriy.algo.cutting

import mail.com.gmail.zayats.andriy.algo.commons.toIndexed
import org.spekframework.spek2.Spek
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertTrue

object BestCuttingTest : Spek({
    
    fun assertCutting(cutting: Cutting, totalPrice: Long, vararg possibleSlices: List<Int>) {
        assertEquals(totalPrice, cutting.totalPrice)
        
        val sortedActualSlices = cutting.slicesLength.sorted()
        
        assertTrue { 
            possibleSlices.any { expectedSlices ->
                expectedSlices.sorted() == sortedActualSlices
            }
        }
    }
    
    group("from book tests") {
        
        val prices = listOf<Long>(0, 1, 5, 8, 9, 10, 17, 17, 20, 24, 30)
            .toIndexed()
            .interval(1, 10)
        
        test("length = 1") {
            val bestCutting = bestCutting(1, prices)
            
            assertCutting(bestCutting, 1, listOf(1))
        }

        test("length = 2") {
            val bestCutting = bestCutting(2, prices)

            assertCutting(bestCutting, 5, listOf(2))
        }
        
        test("length = 3") {
            val bestCutting = bestCutting(3, prices)
            
            assertCutting(bestCutting, 8, listOf(3))
        }

        test("length = 4") {
            val bestCutting = bestCutting(4, prices)

            assertCutting(bestCutting, 10, listOf(2, 2))
        }

        test("length = 5") {
            val bestCutting = bestCutting(5, prices)

            assertCutting(bestCutting, 13, listOf(2, 3))
        }

        test("length = 6") {
            val bestCutting = bestCutting(6, prices)

            assertCutting(bestCutting, 17, listOf(6))
        }

        test("length = 7") {
            val bestCutting = bestCutting(7, prices)

            assertCutting(bestCutting, 18, 
                listOf(1, 6),
                listOf(2, 2, 3)
            )
        }

        test("length = 8") {
            val bestCutting = bestCutting(8, prices)

            assertCutting(bestCutting, 22, listOf(2, 6))
        }

        test("length = 9") {
            val bestCutting = bestCutting(9, prices)

            assertCutting(bestCutting, 25, listOf(3, 6))
        }

        test("length = 10") {
            val bestCutting = bestCutting(10, prices)
            
            assertCutting(bestCutting, 30, listOf(10))
        }
    }

    group("min slice length is 3 and 3-length slice is the most expensive") {

        val prices = listOf<Long>(0, 0, 0, 10, 9)
            .toIndexed()
            .interval(3, 4)

        test("length = 1") {
            val bestCutting = bestCutting(1, prices)

            assertCutting(bestCutting, 0, listOf())
        }

        test("length = 2") {
            val bestCutting = bestCutting(2, prices)

            assertCutting(bestCutting, 0, listOf())
        }

        test("length = 3") {
            val bestCutting = bestCutting(3, prices)

            assertCutting(bestCutting, 10, listOf(3))
        }

        test("length = 4") {
            val bestCutting = bestCutting(4, prices)

            assertCutting(bestCutting, 10, listOf(3))
        }

        test("length = 5") {
            val bestCutting = bestCutting(5, prices)

            assertCutting(bestCutting, 10, listOf(3))
        }

        test("length = 6") {
            val bestCutting = bestCutting(6, prices)

            assertCutting(bestCutting, 20, listOf(3, 3))
        }

        test("length = 7") {
            val bestCutting = bestCutting(7, prices)

            assertCutting(bestCutting, 20, listOf(3, 3))
        }
    }
    
    group("corner cases") {
        
        test("if length = 0 then best cutting is empty") {
            val prices = listOf<Long>(0, 1)
                .toIndexed()
                .interval(1, 1)

            val bestCutting = bestCutting(0, prices)

            assertCutting(bestCutting, 0, listOf())
        }

        test("if min-length slice is longer that length then best cutting is empty") {
            val prices = listOf<Long>(0, 0, 0, 5)
                .toIndexed()
                .interval(3, 3)

            val bestCutting = bestCutting(2, prices)

            assertCutting(bestCutting, 0, listOf())
        }
    }
    
    group("illegal arguments") {
        
        test("min slice length should be 1") {
            val prices = listOf<Long>(0, 1, 2)
                .toIndexed()
                .interval(0, 2)
            
            assertFails { 
                bestCutting(1, prices)
            }
        }
    }
})
