package mail.com.gmail.zayats.andriy.algo.sort.radix

import org.spekframework.spek2.Spek
import kotlin.test.assertFails
import kotlin.test.assertTrue

object RadixSortTest : Spek({
    
    group("test longs sort") {
        
        test("corner values") {
            val input = longArrayOf(
                Long.MAX_VALUE,
                Long.MAX_VALUE - 1,
                256,
                2,
                1,
                0,
                -1,
                -2,
                -256,
                Long.MIN_VALUE + 1,
                Long.MIN_VALUE
            )

            val expected = longArrayOf(
                Long.MIN_VALUE,
                Long.MIN_VALUE + 1,
                -256,
                -2,
                -1,
                0,
                1,
                2,
                256,
                Long.MAX_VALUE - 1,
                Long.MAX_VALUE
            )

            val sorted = radixSort(input)
            assertTrue { expected contentEquals  sorted }
        }
    }

    group("test ints sort") {

        test("corner values") {
            val input = intArrayOf(
                Int.MAX_VALUE,
                Int.MAX_VALUE - 1,
                256,
                2,
                1,
                0,
                -1,
                -2,
                -256,
                Int.MIN_VALUE + 1,
                Int.MIN_VALUE
            )

            val expected = intArrayOf(
                Int.MIN_VALUE,
                Int.MIN_VALUE + 1,
                -256,
                -2,
                -1,
                0,
                1,
                2,
                256,
                Int.MAX_VALUE - 1,
                Int.MAX_VALUE
            )

            val sorted = radixSort(input)
            assertTrue { expected contentEquals  sorted }
        }
    }
    
    group("bytes extractors error cases") {
        
        test("IntBytesExtractor fails when try to extract byte fifth byte (index = 4)") {
            assertFails {
                IntBytesExtractor(0, 4)
            }
        }

        test("LongBytesExtractor fails when try to extract byte ninth byte (index = 8)") {
            assertFails {
                LongBytesExtractor(0, 8)
            }
        }
    }
})
