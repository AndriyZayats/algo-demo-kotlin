package mail.com.gmail.zayats.andriy.algo.sort.counting

import org.junit.jupiter.api.Assertions.assertTrue
import org.spekframework.spek2.Spek
import kotlin.test.assertEquals

object CountingSortSpec : Spek({
    
    group("countingSort") {
        
        test("is stable") {
            val input = arrayOf("BABA", "ABBA", "Z", "A", "B", "ZORRO", "")
            val expected = arrayOf("", "Z", "A", "B", "BABA", "ABBA", "ZORRO")

            val sorted = countingSort(input, 100) {it.length}

            assertTrue { expected contentEquals  sorted }
        }
    }
    
    group("byteArrayCountingSort") {
        
        test("empty array") {
            val sorted = byteArrayCountingSort(ByteArray(0))
            assertTrue {ByteArray(0) contentEquals  sorted}
        }

        test("array of positive and negative and duplicates") {
            val input = byteArrayOf(50, 61, 127, 0, 0, 50, 100, -128, -5)
            val expected = byteArrayOf(-128, -5, 0, 0, 50, 50, 61, 100, 127)

            val sorted = byteArrayCountingSort(input)

            assertTrue {expected contentEquals  sorted}
        }

    }
    
})
