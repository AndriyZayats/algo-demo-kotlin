package mail.com.gmail.zayats.andriy.algo.structures.list.prepending

import org.spekframework.spek2.Spek
import kotlin.test.*

object PrependingListSpec : Spek({

    group("empty list") {

        val emptyList: PrependingList<Nothing> = Empty

        test("is Empty") {
            assertTrue { emptyList is Empty }
        }

        test("is not WithData") {
            assertFalse { emptyList is WithData }
        }

        test("should be equal to other empty list") {
            assertEquals(Empty, emptyList)
        }

        test("should be different from other non empty list") {
            assertNotEquals(prependingListOf("Some"), emptyList)
        }

        test("iterator should not contain elements") {
            assertFalse { emptyList.iterator().hasNext() }
        }

        test("size should be zero") {
            assertEquals(0, emptyList.calculateSize())
        }

        test("reverse should be empty list") {
            assertEquals(Empty, emptyList.reverse())
        }
    }

    group("single element list") {

        val singleValue = "Single"

        val singleValueList = prependingListOf(singleValue)

        test("is not Empty") {
            assertFalse { singleValueList is Empty }
        }

        test("is WithData") {
            assertTrue { singleValueList is WithData }
        }

        test("head is single element") {
            val head = (singleValueList as WithData).head
            assertEquals(singleValue, head)
        }

        test("tail is Empty") {
            val tail = (singleValueList as WithData).tail
            assertEquals(Empty, tail)
        }

        test("should be equal to the same list") {
            assertEquals(prependingListOf(singleValue), singleValueList)
        }

        test("should be different from empty list") {
            assertNotEquals(Empty, singleValueList)
        }

        test("should be different from other single element list") {
            assertNotEquals(prependingListOf("Other"), singleValueList)
        }

        test("should be different from other list with more elements") {
            assertNotEquals(prependingListOf(singleValue, "OneMoreValue"), singleValueList)
        }

        test("iterator should contain single element only") {
            val iterator = singleValueList.iterator()
            assertTrue { iterator.hasNext() }
            assertEquals(singleValue, iterator.next())
            assertFalse { iterator.hasNext() }
        }

        test("size should be one") {
            assertEquals(1, singleValueList.calculateSize())
        }

        test("reverse should be equal to source") {
            assertEquals(singleValueList, singleValueList.reverse())
        }
    }

    group("few elements list") {

        val firstValue = "F"
        val secondValue = "S"
        val thirdValue = "T"

        val list = prependingListOf(firstValue, secondValue, thirdValue)

        test("is not Empty") {
            assertFalse { list is Empty }
        }

        test("is WithData") {
            assertTrue { list is WithData }
        }

        test("head is first element") {
            val head = (list as WithData).head
            assertEquals(firstValue, head)
        }

        test("tail is sublist from elements except head") {
            val tail = (list as WithData).tail
            assertEquals(prependingListOf(secondValue, thirdValue), tail)
        }

        test("should be equal to the same list") {
            assertEquals(prependingListOf(firstValue, secondValue, thirdValue), list)
        }

        test("should be different from empty list") {
            assertNotEquals(Empty, list)
        }

        test("iterator should contain all elements") {
            val iterator = list.iterator()
            assertTrue { iterator.hasNext() }
            assertEquals(firstValue, iterator.next())
            assertTrue { iterator.hasNext() }
            assertEquals(secondValue, iterator.next())
            assertTrue { iterator.hasNext() }
            assertEquals(thirdValue, iterator.next())
            assertFalse { iterator.hasNext() }
        }

        test("size should be equal to elements count") {
            assertEquals(3, list.calculateSize())
        }

        test("in reverse elements should be in opposite order") {
            val reversed = prependingListOf(thirdValue, secondValue, firstValue)
            assertEquals(reversed, list.reverse())
        }
    }

    group("operation prependTo") {

        val a = 'A'
        val b = 'B'
        val c = 'C'

        test("'A' prepend to empty list = (A)") {
            val actual = a prependTo Empty
            val expected = prependingListOf(a)
            assertEquals(expected, actual)
        }

        test("'A' prepend to ('B') = ('A', 'B')") {
            val actual = a prependTo prependingListOf(b)
            val expected = prependingListOf(a, b)
            assertEquals(expected, actual)
        }

        test("'A' prepend to ('B' prepend to ('C' prepend to empty)) = ('A', 'B', 'C')") {
            val actual = a prependTo (b prependTo (c prependTo Empty))
            val expected = prependingListOf(a, b, c)
            assertEquals(expected, actual)
        }

        test("'A' prepend to reversed('B', 'C') = ('A', 'C', 'B')") {
            val actual = a prependTo prependingListOf(b, c).reverse()
            val expected = prependingListOf(a, c, b)
            assertEquals(expected, actual)
        }
    }
    
    group("iterator") {

        val firstValue = "F"
        val secondValue = "S"
        val thirdValue = "T"

        val list = prependingListOf(firstValue, secondValue, thirdValue)

        test("independent iterators") {

            val iterator1 = list.iterator()
            val iterator2 = list.iterator()

            assertTrue { iterator1.hasNext() }
            assertEquals(firstValue, iterator1.next())
            assertTrue { iterator1.hasNext() }
            assertEquals(secondValue, iterator1.next())

            assertTrue { iterator1.hasNext() }
            assertEquals(firstValue, iterator2.next())
            assertTrue { iterator1.hasNext() }
            assertEquals(secondValue, iterator2.next())
            assertTrue { iterator1.hasNext() }
            assertEquals(thirdValue, iterator2.next())

            assertTrue { iterator1.hasNext() }
            assertEquals(thirdValue, iterator1.next())

            assertFalse { iterator1.hasNext() }
            assertFalse { iterator2.hasNext() }
        }
        
        test("next throws exception when there are no more elements") {
            
            val iterator = list.iterator()
            
            iterator.next()
            iterator.next()
            iterator.next()

            assertFails {
                iterator.next()
            }
        }
    }

    group("toString") {

        test("of empty equals to '()'") {
            assertEquals("()", Empty.toString())
        }

        test("of ('A', 'B', 'C') equals to '(A, B, C)'") {
            assertEquals("(A, B, C)", prependingListOf('A', 'B', 'C').toString())
        }
    }
})
