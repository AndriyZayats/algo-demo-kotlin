package mail.com.gmail.zayats.andriy.algo.sequences.lcs

import org.spekframework.spek2.Spek
import kotlin.test.assertEquals

object LongestCommonSubsequenceTest : Spek({
    
    group("longestCommonSubString") {

        test("lcs of two strings") {
            val a = "ABCDEFGHIJKLMOPQRSTUVWXY11a22Z"
            val b = "1A2B3C4D5E6F7G8H9I0J-=qwOePrQtRySuTiUoVpW[X]YaZs"
            val expected = "ABCDEFGHIJOPQRSTUVWXYaZ"

            val actual = longestCommonSubString(a, b)

            assertEquals(expected, actual)
        }

        test("lcs of two empty = empty") {
            assertEquals("", longestCommonSubString("", ""))
        }

        test("lcs of some and empty = empty") {
            assertEquals("", longestCommonSubString("some", ""))
            assertEquals("", longestCommonSubString("", "some"))
        }

        test("lcs of string with unique chars and it reversed = single letter") {
            assertEquals(1, longestCommonSubString("abcd", "dcba").length)
        }

        test("lcs of strings with duplicated chars") {
            assertEquals("bbb", longestCommonSubString("aabbbc", "cbbbaa"))
        }

        test("lcs of strings without equal characters is empty") {
            assertEquals("", longestCommonSubString("123456", "abc"))
        }
    }
})
