package mail.com.gmail.zayats.andriy.algo.operations.power.quick

import org.spekframework.spek2.Spek
import java.math.BigInteger
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

object QuickPowerSpec : Spek({
    
    group("limitations") {
        
        test("negative power is forbidden") {
            assertFailsWith(IllegalArgumentException::class) {
                BigInteger.TEN power -1
            }
        }
    }

    test("assume 0 ^ 0 = 1") {
        assertEquals(BigInteger.valueOf(1), BigInteger.valueOf(0) power 0)
    }

    group("calculation") {
        
        test("4 ^ 3 = 64") {
            assertEquals(BigInteger.valueOf(64), BigInteger.valueOf(4) power 3)
        }

        test("2 ^ 10 = 1024") {
            assertEquals(BigInteger.valueOf(1024), BigInteger.valueOf(2) power 10)
        }

        test("10 ^ 11 = 100000000000") {
            assertEquals(BigInteger.valueOf(100000000000), BigInteger.valueOf(10) power 11)
        }

        test("8 ^ 1 = 8") {
            assertEquals(BigInteger.valueOf(8), BigInteger.valueOf(8) power 1)
        }

        test("9 ^ 0 = 1") {
            assertEquals(BigInteger.valueOf(1), BigInteger.valueOf(9) power 0)
        }
    }

    group("verification against iterative power") {
        
        fun iterativePower(x: BigInteger, pow: Long): BigInteger {
            var result = BigInteger.ONE
            for (i in 1..pow) {
                result *= x
            }
            return result
        }
        
        fun verify(x: BigInteger, pow: Long) {
            assertEquals(iterativePower(x, pow), x power pow)
        }

        test("3 ^ 1023") {
            verify(BigInteger.valueOf(3), 1023)
        }

        test("3 ^ 1024") {
            verify(BigInteger.valueOf(3), 1024)
        }

        test("10 ^ 11 = 100000000000") {
            assertEquals(BigInteger.valueOf(100000000000), BigInteger.valueOf(10) power 11)
        }

        test("8 ^ 1 = 8") {
            assertEquals(BigInteger.valueOf(8), BigInteger.valueOf(8) power 1)
        }

        test("9 ^ 0 = 1") {
            assertEquals(BigInteger.valueOf(1), BigInteger.valueOf(9) power 0)
        }
    }

})
