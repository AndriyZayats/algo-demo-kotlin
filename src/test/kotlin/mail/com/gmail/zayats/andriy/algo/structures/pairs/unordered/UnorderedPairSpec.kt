package mail.com.gmail.zayats.andriy.algo.structures.pairs.unordered

import org.junit.jupiter.api.Assertions.assertFalse
import org.spekframework.spek2.Spek
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertNull

object UnorderedPairSpec : Spek({
    
    group("UnorderedPair") {
        
        test("should provide access to it elements") {
            val pair = UnorderedPair(1, 2)
            
            assertEquals(1, pair.a)
            assertEquals(2, pair.b)
        }

        test("should be equal to the pair created in the same way") {
            val pair = UnorderedPair(3, 5)
            val other = UnorderedPair(3, 5)

            assertEquals(other, pair)
            assertEquals(other.hashCode(), pair.hashCode())
        }

        test("should be equal to the pair created in swapped way") {
            val pair = UnorderedPair(3, 5)
            val other = UnorderedPair(5, 3)

            assertEquals(other, pair)
            assertEquals(other.hashCode(), pair.hashCode())
        }

        test("with equal elements should be equal to the same pair") {
            val pair = UnorderedPair(7, 7)
            val other = UnorderedPair(7, 7)

            assertEquals(other, pair)
            assertEquals(other.hashCode(), pair.hashCode())
        }

        test("should be equal to itself") {
            val pair = UnorderedPair(1, 2)

            assertEquals(pair, pair)
        }

        test("should be different if any of parameters are different") {
            val pair = UnorderedPair(1, 2)
            val other1 = UnorderedPair(1, 100)
            val other2 = UnorderedPair(100, 2)
            val other3 = UnorderedPair(100, 1)
            val other4 = UnorderedPair(2, 100)
            val other5 = UnorderedPair(200, 300)

            assertNotEquals(other1, pair)
            assertNotEquals(other2, pair)
            assertNotEquals(other3, pair)
            assertNotEquals(other4, pair)
            assertNotEquals(other5, pair)
        }

        test("should be different to other lass") {
            val pair = UnorderedPair(1, 2)

            assertFalse { pair.equals(Object()) }
        }

        test("should be different to null") {
            val pair = UnorderedPair(1, 2)

            assertFalse { pair.equals(null) }
        }

        group("of nullable") {

            test("should provide access to it elements") {
                val pair = UnorderedPair(null, null)

                assertNull(pair.a)
                assertNull(pair.b)
            }

            test("should be equal to the pair created in the same way") {
                val pair = UnorderedPair(3, null)
                val other = UnorderedPair(3, null)

                assertEquals(other, pair)
                assertEquals(other.hashCode(), pair.hashCode())
            }

            test("should be equal to the pair created in swapped way") {
                val pair = UnorderedPair(3, null)
                val other = UnorderedPair(null, 3)

                assertEquals(other, pair)
                assertEquals(other.hashCode(), pair.hashCode())
            }

            test("created with both nulls should be equal to the same pair") {
                val pair = UnorderedPair(null, null)
                val other = UnorderedPair(null, null)

                assertEquals(other, pair)
                assertEquals(other.hashCode(), pair.hashCode())
            }
        }
    }
})
