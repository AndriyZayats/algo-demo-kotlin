package mail.com.gmail.zayats.andriy.algo.interval.sum.max

import org.spekframework.spek2.Spek
import kotlin.test.assertEquals

object IntervalWithMaxSumTest : Spek({

    group("intervalWithMaxSum") {

        fun execute(vararg numbers: Int) =
            intsIntervalWithMaxSum(numbers.toList())

        test("returns interval with max sum") {
            val actual = execute(1, 2, 3, -6, 1, 1, 2, 3)
            val expected = IntervalSumData(
                sum = 7,
                from = 4,
                to = 8
            )
            assertEquals(expected, actual)
        }

        test("returns interval with max sum even if interval is not the longest positive or increasing") {
            val actual = execute(1, 2, 3, -6, 4, 5)
            val expected = IntervalSumData(
                sum = 9,
                from = 4,
                to = 6
            )
            assertEquals(expected, actual)
        }

        test("result can include negative values inside max sum interval") {
            val actual = execute(5, -3, 10)
            val expected = IntervalSumData(
                sum = 12,
                from = 0,
                to = 3
            )
            assertEquals(expected, actual)
        }

        test("should return first from all possible results") {
            val actual = execute(0, 0, 1, 1, 0, 0, -2, 2, -2, 1, 1)
            val expected = IntervalSumData(
                sum = 2,
                from = 2,
                to = 4
            )
            assertEquals(expected, actual)
        }

        group("edge cases") {

            val firstEmpty =
                IntervalSumData(
                    sum = 0,
                    from = 0,
                    to = 0
                )

            test("for empty list should return first empty result") {
                val actual = execute()
                assertEquals(firstEmpty, actual)
            }

            test("for all negative and zero elements should return first empty result") {
                val actual = execute(-4, 0, -10, -1)
                assertEquals(firstEmpty, actual)
            }

            test("for single positive element should return this element") {
                val positiveElement = 5
                val actual = execute(positiveElement)
                val expected = IntervalSumData(
                    sum = positiveElement,
                    from = 0,
                    to = 1
                )
                assertEquals(expected, actual)
            }

            test("for all positive elements should return whole input data") {
                val actual = execute(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                val expected = IntervalSumData(
                    sum = 55,
                    from = 0,
                    to = 10
                )
                assertEquals(expected, actual)
            }
        }
    }
})
