package mail.com.gmail.zayats.andriy.algo.structures.functions.polynomial

import org.spekframework.spek2.Spek
import kotlin.test.assertEquals

object PolynomialSpec : Spek({
    
    group("degree") {

        test("should be highest degree of non zero terms") {
            val p = Polynomial(0.0, 1.1, 2.2, 3.3, 4.4, 5.5, 0.0, 0.0)
            assertEquals(5, p.degree)
        }

        test("of constant is zero even if constant is zero") {
            val p = Polynomial(0.0)
            assertEquals(0, p.degree)
        }
    }

    group("calculate") {

        test("constant") {
            val a0 = 5.0
            val p = Polynomial(a0)
            assertEquals(a0, p(100.0))
        }

        test("zero constant") {
            val a0 = 0.0
            val p = Polynomial(a0)
            assertEquals(a0, p(100.0))
        }

        test("linear") {
            val a0 = -5.0
            val a1 = 2.0
            val x = 10.0
            val p = Polynomial(a0, a1)
            assertEquals(a1 * x + a0, p(x))
        }

        test("quadratic") {
            val a0 = -5.0
            val a1 = 2.0
            val a2 = -3.5
            val x = 10.0
            val p = Polynomial(a0, a1, a2)
            assertEquals(a2 * x * x + a1 * x + a0, p(x))
        }

        test("cubic") {
            val a0 = -5.0
            val a1 = 2.0
            val a2 = -3.5
            val a3 = -300.2
            val x = 10.0
            val p = Polynomial(a0, a1, a2, a3)
            assertEquals(a3 * x * x * x + a2 * x * x + a1 * x + a0, p(x))
        }
    }

    group("toString") {
        
        test("terms should be in descending degree order (-4.5*x^3 + 3.3*x^2 - 2.2*x + 1.1)") {
            val p = Polynomial(1.1, -2.2, 3.3, -4.5)
            assertEquals("-4.5*x^3 + 3.3*x^2 - 2.2*x + 1.1", p.toString())
        }

        test("argument name is configurable (3.3*y^2 - 2.2*y + 1.1)") {
            val p = Polynomial(1.1, -2.2, 3.3)
            assertEquals("3.3*y^2 - 2.2*y + 1.1", p.toString('y'))
        }

        test("zero terms are skipped (5.0*x^3 - 2.2*x)") {
            val p = Polynomial(0.0, -2.2, 0.0, 5.0)
            assertEquals("5.0*x^3 - 2.2*x", p.toString())
        }
    }
})
