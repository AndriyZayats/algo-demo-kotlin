package mail.com.gmail.zayats.andriy.algo

import mail.com.gmail.zayats.andriy.algo.structures.pairs.unordered.UnorderedPair
import org.spekframework.spek2.Spek
import kotlin.math.exp
import kotlin.test.assertEquals
import kotlin.test.assertTrue

object AllIntPairsWithSumSpec : Spek({
    
    group("findAllIntPairsWithSum") {

        test("should return correct result") {
            val actual = findAllIntPairsWithSum(10, listOf(-5, 15, 20, 0, 10, 0, 3, 7, 6, 5, -30))
            val expected = setOf(
                UnorderedPair(-5, 15),
                UnorderedPair(0, 10),
                UnorderedPair(3, 7)
            )
            assertEquals(expected, actual)
        }

        test("should return empty result for empty collection") {
            val actual = findAllIntPairsWithSum(10, listOf())
            assertTrue { actual.isEmpty() }
        }

        test("should return empty result for collection from one element") {
            val actual = findAllIntPairsWithSum(10, listOf(10))
            assertTrue { actual.isEmpty() }
        }
    }
})
