package mail.com.gmail.zayats.andriy.algo.commons

import org.spekframework.spek2.Spek
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertFalse
import kotlin.test.assertTrue

object IndexedTest : Spek({
    
    group("IndexedList") {
        
        val list = listOf("a", "b", "c")
        
        val indexed = list.toIndexed()
        
        test("get(i)") {
            assertEquals("a", indexed[0])
            assertEquals("b", indexed[1])
            assertEquals("c", indexed[2])
        }

        test("iterable") {
            assertEquals(list, indexed.toList())
        }
    }

    group("IndexedString") {

        val string = "abc"

        val indexed = string.toIndexed()

        test("get(i)") {
            assertEquals('a', indexed[0])
            assertEquals('b', indexed[1])
            assertEquals('c', indexed[2])
        }

        test("iterable and asString") {
            assertEquals(string, indexed.asString())
        }
    }
    
    test("iterator") {
        val string = "ab"
        val indexed = string.toIndexed()
        val iterator = indexed.iterator()
        
        assertTrue { iterator.hasNext() }
        assertEquals('a', iterator.next())
        assertTrue { iterator.hasNext() }
        assertEquals('b', iterator.next())
        assertFalse { iterator.hasNext() }
        assertFails { 
            iterator.next()
        }
    }

    group("interval") {

        val string = "abcd"
        val indexed = string.toIndexed()
        
        test("returned interval is correct") {
            val requestedIndexOfFirst = 1
            val requestedIndexOfLast = 2
            
            val interval = indexed.interval(requestedIndexOfFirst, requestedIndexOfLast)

            assertEquals(requestedIndexOfFirst, interval.indexOfFirst)
            assertEquals(requestedIndexOfLast, interval.indexOfLast)
            assertEquals('b', interval[1])
            assertEquals('c', interval[2])
            val indexesInterval = requestedIndexOfFirst..requestedIndexOfLast
            assertEquals(indexesInterval, interval.indexesInterval)
            assertEquals(indexesInterval.count(), interval.length)
        }

        test("returned whole interval is correct") {
            val requestedIndexOfFirst = 0
            val requestedIndexOfLast = indexed.size - 1

            val interval = indexed.interval(requestedIndexOfFirst, requestedIndexOfLast)

            assertEquals(requestedIndexOfFirst, interval.indexOfFirst)
            assertEquals(requestedIndexOfLast, interval.indexOfLast)
            assertEquals('a', interval[0])
            assertEquals('b', interval[1])
            assertEquals('c', interval[2])
            assertEquals('d', interval[3])
            val indexesInterval = requestedIndexOfFirst..requestedIndexOfLast
            assertEquals(indexesInterval, interval.indexesInterval)
            assertEquals(indexesInterval.count(), interval.length)
        }

        test("returned interval from one element is correct") {
            val elementIndex = 1

            val interval = indexed.interval(elementIndex, elementIndex)

            assertEquals(elementIndex, interval.indexOfFirst)
            assertEquals(elementIndex, interval.indexOfLast)
            assertEquals(indexed[elementIndex], interval[elementIndex])
            assertEquals(elementIndex..elementIndex, interval.indexesInterval)
            assertEquals(1, interval.length)
        }

        test("requesting less than 0 throws exception") {
            assertFails {
                indexed.interval(-1, 1)
            }
        }

        test("requesting more than exist throws exception") {
            assertFails {
                indexed.interval(0, indexed.size)
            }
        }

        test("requesting to < from throws exception") {
            assertFails {
                indexed.interval(1, 0)
            }
        }
    }

})
