package mail.com.gmail.zayats.andriy.algo.structures.heap.mutable

import org.spekframework.spek2.Spek
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertFalse
import kotlin.test.assertTrue

object MutableHeapSpec : Spek({

    val intCollection = listOf(30, 15, 0, -4, 50, -10, 18, 0)
    val stringCollection = listOf("AAA", "BB", "C", "", "ZZZ", "ZZZ", "ZAB", "ZA")

    group("heap insert") {
        val heap by memoized { MutableHeap<Int> { a, b -> a < b } }

        test("should increase size") {
            assertEquals(0, heap.size)

            heap.insert(0)
            assertEquals(1, heap.size)
        }

        test("first should become extreme") {
            heap.insert(5)
            assertEquals(5, heap.getExtreme())
        }

        test("second dominant should become extreme") {
            heap.insert(5)
            heap.insert(4)
            assertEquals(4, heap.getExtreme())
        }

        test("second no dominant shouldn't become extreme") {
            heap.insert(5)
            heap.insert(6)
            assertEquals(5, heap.getExtreme())
        }
    }

    group("heap with inserted elements") {
        val heap by memoized {
            val result = MutableHeap<Int> { a, b -> a < b }
            intCollection.forEach {
                result.insert(it)
            }
            return@memoized result
        }

        test("with min domination should have ascending extremes") {

            val sortedList = intCollection.sorted()

            sortedList.forEach {
                assertEquals(it, heap.takeExtreme())
            }
        }

        test("should have correct size") {

            assertEquals(intCollection.size, heap.size)
        }

        test("remove extreme should decrease size") {

            for (i in intCollection.size downTo 1) {
                heap.removeExtreme()
                assertEquals(i - 1, heap.size)
            }
        }
    }


    group("heap created from collection") {

        val heap by memoized {
            MutableHeap({ a, b -> a < b }, intCollection)
        }

        test("with min domination should have ascending extremes") {

            val sortedList = intCollection.sorted()

            sortedList.forEach {
                assertEquals(it, heap.takeExtreme())
            }
        }

        test("should have the same size as collection") {

            assertEquals(intCollection.size, heap.size)
        }

        test("remove extreme should decrease size") {

            for (i in intCollection.size downTo 1) {
                heap.removeExtreme()
                assertEquals(i - 1, heap.size)
            }
        }
    }

    group("isEmpty") {

        test("of heap created from empty collection should be true") {

            val emptyHeap = MutableHeap<String>({ a, b -> a < b }, emptyList())
            assertTrue { emptyHeap.isEmpty }
        }

        test("of heap created from not empty collection should be false") {

            val heap = MutableHeap({ a, b -> a < b }, listOf("A"))
            assertFalse { heap.isEmpty }
        }

        test("of just created heap should be true") {

            val heap = MutableHeap<String> { a, b -> a < b }
            assertTrue { heap.isEmpty }
        }

        test("of created heap with added element should be false") {

            val heap = MutableHeap<String> { a, b -> a < b }
            heap.insert("O")

            assertFalse { heap.isEmpty }
        }

        test("of created heap with added and removed element should be true") {

            val heap = MutableHeap<String> { a, b -> a < b }
            heap.insert("O")
            heap.removeExtreme()

            assertTrue { heap.isEmpty }
        }
    }

    group("construct functions") {

        test("minHeap(elements) should have minimum extreme") {
            val heap = minHeap(2, 1, 0, 3)
            assertEquals(0, heap.getExtreme())
        }

        test("maxHeap(elements) should have maximum extreme") {
            val heap = maxHeap(2, 1, 0, 3)
            assertEquals(3, heap.getExtreme())
        }

        test("minHeap(collection) should have minimum extreme") {
            val heap = minHeap(intCollection)
            assertEquals(intCollection.min(), heap.getExtreme())
        }

        test("maxHeap(collection) should have maximum extreme") {
            val heap = maxHeap(intCollection)
            assertEquals(intCollection.max(), heap.getExtreme())
        }

        test("minHeap(comparator) should have minimum extreme") {
            val comparator: Comparator<String> = Comparator
                .comparing<String, Int> { it.length }
                .thenComparing<String> { it }

            val heap = minHeap(comparator)
            stringCollection.forEach {
                heap.insert(it)
            }

            assertEquals(stringCollection.minWith(comparator), heap.getExtreme())
        }

        test("maxHeap(comparator) should have maximum extreme") {
            val comparator: Comparator<String> = Comparator
                .comparing<String, Int> { it.length }
                .thenComparing<String> { it }

            val heap = maxHeap(comparator)
            stringCollection.forEach {
                heap.insert(it)
            }

            assertEquals(stringCollection.maxWith(comparator), heap.getExtreme())
        }
    }

    group("takeExtreme") {

        test("returns extreme and remove it") {
            val heap = maxHeap(listOf(9, 10))
            
            val firstExtreme = heap.takeExtreme()
            val secondExtreme = heap.getExtreme()
            
            assertEquals(10, firstExtreme)
            assertEquals(9, secondExtreme)
            assertEquals(1, heap.size)
        }
    }

    group("heap violations") {

        val emptyHeap by memoized { minHeap<String>() }

        test("getExtreme from empty heap") {
            assertFailsWith<NoSuchElementException> { 
                emptyHeap.getExtreme() 
            }
        }

        test("removeExtreme from empty heap") {
            assertFailsWith<NoSuchElementException> {
                emptyHeap.removeExtreme()
            }
        }
    }
})
