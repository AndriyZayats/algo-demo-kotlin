package mail.com.gmail.zayats.andriy.algo.interval.sum.max

data class IntervalSumData<V>(
    val sum: V,
    val from: Int,
    val to: Int
)

interface DomainDescriptor<V : Comparable<V>> {

    val zeroValue: V

    infix fun V.add(element: V): V
}

fun <V : Comparable<V>> intervalWithMaxSum(data: Iterable<V>, rules: DomainDescriptor<V>): IntervalSumData<V> =
    with(rules) {
        
        operator fun V.plus(other: V) = this add other

        fun maxOrFirst(first: IntervalSumData<V>, second: IntervalSumData<V>) =
            if (second.sum > first.sum) second else first

        fun newBestNotNegativeTail(notNegativeTail: IntervalSumData<V>, nextElement: V): IntervalSumData<V> {
            val nextIndex = notNegativeTail.to + 1
            val newAppendedTailValue = notNegativeTail.sum.add(nextElement)
            val newAppendedTail = IntervalSumData(newAppendedTailValue, notNegativeTail.from, nextIndex)
            val newEmptyTail = IntervalSumData(zeroValue, nextIndex, nextIndex)

            return maxOrFirst(newEmptyTail, newAppendedTail)
        }

        tailrec fun seek(
            iterator: Iterator<V>,
            currentIndex: Int,
            previousBest: IntervalSumData<V>,
            previousNotNegativeTail: IntervalSumData<V>
        ): IntervalSumData<V> {

            if (iterator.hasNext()) {
                val element = iterator.next()
                val currentBestNotNegativeTail = newBestNotNegativeTail(previousNotNegativeTail, element)
                val currentBest = maxOrFirst(previousBest, currentBestNotNegativeTail)
                return seek(iterator, currentIndex + 1, currentBest, currentBestNotNegativeTail)
            } else {
                return previousBest
            }
        }

        val firstEmptySubSequenceData = IntervalSumData(rules.zeroValue, 0, 0)

        return seek(data.iterator(), 0, firstEmptySubSequenceData, firstEmptySubSequenceData)
    }
