package mail.com.gmail.zayats.andriy.algo.structures.functions.polynomial

import mail.com.gmail.zayats.andriy.algo.structures.functions.SingleArgumentFunction
import kotlin.math.abs
import kotlin.math.max

class Polynomial(coefficients: List<Double>) : SingleArgumentFunction {

    constructor(vararg coefficients: Double) : this(coefficients.toList())

    private val a: DoubleArray

    init {
        val lastNonZeroIndex = coefficients.map { it != 0.0 }.lastIndexOf(true)
        val degree = max(lastNonZeroIndex, 0)
        a = coefficients.subList(0, degree + 1).toDoubleArray()
    }

    val degree
        get() = a.size - 1

    override fun invoke(argument: Double): Double {
        return calculatePolynomialByHorner(argument, a)
    }

    override fun toString(argumentName: Char) = polynomialString(a, argumentName)

    override fun toString() = toString('x')
}

private fun polynomialString(a: DoubleArray, argumentName: Char): String {
    val sb = StringBuilder()
    for (i in a.size - 1 downTo 0) {
        val ai = a[i]
        if (ai == 0.0) {
            continue
        }
        if (sb.isEmpty()) {
            sb.append(ai)
        } else {
            sb.append(if (ai >= 0.0) " + " else " - ")
            sb.append(abs(ai))
        }
        if (i > 0) {
            sb.append('*')
            sb.append(argumentName)
            if (i > 1) {
                sb.append('^')
                    .append(i)
            }
        }
    }
    return sb.toString()
}

private fun calculatePolynomialByHorner(x: Double, a: DoubleArray): Double {
    // Horner's method
    return a.foldRight(0.0) { ai, result ->
        result * x + ai
    }
}
