package mail.com.gmail.zayats.andriy.algo

fun findAllIntPairsWithSum(requiredSum: Int, elements: Iterable<Int>) =
    findAllPairsWithSum(requiredSum, elements) { a, b -> a + b }
