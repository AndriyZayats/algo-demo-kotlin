package mail.com.gmail.zayats.andriy.algo.commons

import java.lang.IllegalArgumentException

interface Indexed<T> : Iterable<T> {

    operator fun get(i: Int): T

    val size: Int

    override fun iterator(): Iterator<T> = IndexedIterator(this)

    fun interval(from: Int, to: Int): IndexedInterval<T> {
        if (from < 0) {
            throw IllegalArgumentException()
        }
        if (to >= size) {
            throw IllegalArgumentException()
        }
        if (to < from) {
            throw IllegalArgumentException()
        }
        return object : IndexedInterval<T> {
            override fun get(i: Int): T =
                this@Indexed[i]

            override val length: Int
                get() = to - from + 1
            override val indexOfFirst: Int
                get() = from
            override val indexOfLast: Int
                get() = to

        }
    }
}

interface IndexedInterval<T> {

    operator fun get(i: Int): T

    val length: Int

    val indexOfFirst: Int

    val indexOfLast: Int

    val indexesInterval
        get() = indexOfFirst..indexOfLast
}

private class IndexedIterator<T>(private val indexed: Indexed<T>) : Iterator<T> {

    private var currentIndex = 0

    override fun hasNext() = currentIndex < indexed.size

    override fun next(): T =
        if (hasNext()) {
            currentIndex++
            indexed[currentIndex - 1]
        } else {
            throw IllegalStateException()
        }
}

private class IndexedList<T>(private val list: List<T>) : Indexed<T> {

    override val size = list.size

    override fun get(i: Int) = list[i]
}

fun <T> List<T>.toIndexed(): Indexed<T> = IndexedList(this)

private class IndexedString(private val str: String) : Indexed<Char> {

    override val size = str.length

    override fun get(i: Int) = str[i]
}

fun String.toIndexed(): Indexed<Char> = IndexedString(this)

fun Indexed<Char>.asString() = this.joinToString(separator = "")
