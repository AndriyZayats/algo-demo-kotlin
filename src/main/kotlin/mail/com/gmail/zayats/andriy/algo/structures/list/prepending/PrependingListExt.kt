package mail.com.gmail.zayats.andriy.algo.structures.list.prepending

fun PrependingList<*>.calculateSize(): Int = accumulateListSize(this, 0)

fun <T> PrependingList<T>.reverse(): PrependingList<T> = accumulateReverse(this, Empty)

private tailrec fun accumulateListSize(list: PrependingList<*>, acc: Int): Int =
    when (list) {
        is Empty -> acc
        is WithData -> accumulateListSize(list.tail, acc + 1)
    }

private tailrec fun <T> accumulateReverse(toReverse: PrependingList<T>, acc: PrependingList<T>): PrependingList<T> =
    when (toReverse) {
        is Empty -> acc
        is WithData -> accumulateReverse(toReverse.tail, toReverse.head prependTo acc)
    }
