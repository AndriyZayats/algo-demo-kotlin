package mail.com.gmail.zayats.andriy.algo.sequences.lcs

import mail.com.gmail.zayats.andriy.algo.commons.asString
import mail.com.gmail.zayats.andriy.algo.commons.toIndexed

fun longestCommonSubString(a: String, b: String) = 
    longestCommonSubSequence(a.toIndexed(), b.toIndexed()).asString()
