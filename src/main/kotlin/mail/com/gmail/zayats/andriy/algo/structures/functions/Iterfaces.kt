package mail.com.gmail.zayats.andriy.algo.structures.functions

interface SingleArgumentFunction {
    
    operator fun invoke(argument: Double): Double

    fun toString(argumentName: Char): String
}
