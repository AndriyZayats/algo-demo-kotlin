package mail.com.gmail.zayats.andriy.algo.sort.radix

import java.lang.IllegalArgumentException

fun <T> radixSortByLongKey(input: Array<T>, keyExtractor: (T) -> Long) =
    radixSort(input, 8) { element, byteNumber -> 
        LongBytesExtractor(keyExtractor(element), byteNumber) 
    }

fun <T> radixSortByIntKey(input: Array<T>, keyExtractor: (T) -> Int) =
    radixSort(input, 4) { element, byteNumber ->
        IntBytesExtractor(keyExtractor(element), byteNumber)
    }

fun radixSort(input: LongArray) =
    radixSortByLongKey(input.toTypedArray()) { it }.toLongArray()

fun radixSort(input: IntArray) =
    radixSortByIntKey(input.toTypedArray()) { it }.toIntArray()

object LongBytesExtractor : (Long, Int) -> UByte {

    override fun invoke(longKey: Long, byteNumber: Int): UByte =
        when (byteNumber) {
            0 -> (longKey and 0xFF).toUByte()
            1 -> (longKey shr 8 and 0xFF).toUByte()
            2 -> (longKey shr 16 and 0xFF).toUByte()
            3 -> (longKey shr 24 and 0xFF).toUByte()
            4 -> (longKey shr 32 and 0xFF).toUByte()
            5 -> (longKey shr 40 and 0xFF).toUByte()
            6 -> (longKey shr 48 and 0xFF).toUByte()
            7 -> ((longKey shr 56 and 0xFF) + 0x80).toUByte()
            else -> throw IllegalArgumentException()
        }
}

object IntBytesExtractor : (Int, Int) -> UByte {

    override fun invoke(intKey: Int, byteNumber: Int): UByte =
        when (byteNumber) {
            0 -> (intKey and 0xFF).toUByte()
            1 -> (intKey shr 8 and 0xFF).toUByte()
            2 -> (intKey shr 16 and 0xFF).toUByte()
            3 -> ((intKey shr 24 and 0xFF) + 0x80).toUByte()
            else -> throw IllegalArgumentException()
        }
}
