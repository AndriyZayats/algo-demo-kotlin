package mail.com.gmail.zayats.andriy.algo.structures.pairs.unordered

class UnorderedPair<E>(val a: E, val b: E) {

    override fun equals(other: Any?): Boolean {

        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UnorderedPair<*>

        if (a == other.a && b == other.b) return true
        if (a == other.b && b == other.a) return true

        return false
    }

    override fun hashCode(): Int {
        val aHash = a?.hashCode() ?: 0
        val bHash = b?.hashCode() ?: 0
        return aHash + bHash
    }
}
