package mail.com.gmail.zayats.andriy.algo.interval.sum.max

private object IntDescriptor : DomainDescriptor<Int> {

    override val zeroValue = 0

    override fun Int.add(element: Int) = this + element

}

fun intsIntervalWithMaxSum(data: Iterable<Int>) =
    intervalWithMaxSum(
        data,
        IntDescriptor
    )
