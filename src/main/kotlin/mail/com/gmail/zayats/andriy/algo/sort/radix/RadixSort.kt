package mail.com.gmail.zayats.andriy.algo.sort.radix

import mail.com.gmail.zayats.andriy.algo.sort.counting.countingSort

fun <T> radixSort(input: Array<T>, bytesCount: Int, radix256byteExtractor: (T, Int) -> UByte): Array<T> {

    infix fun T.byte(n: Int) = radix256byteExtractor(this, n)

    var result = input
    for (i in 0 until bytesCount) {
        result = countingSort(result, 256) {
            (it byte i).toInt()
        }
    }
    return result
}


