package mail.com.gmail.zayats.andriy.algo.operations.power.quick

import java.math.BigInteger

infix fun BigInteger.power(pow: Long): BigInteger {
    if (pow < 0) {
        throw IllegalArgumentException("Negative power can cause domination of evil :)")
    }
    return quickPower(this, pow)
}

private fun quickPower(x: BigInteger, pow: Long): BigInteger {
    if (pow == 0L) {
        return BigInteger.ONE
    } else {
        val isEven = (pow and 1L) == 0L
        if (isEven) {
            val square = quickPower(x, pow / 2)
            return square * square
        } else {
            return x * quickPower(x, pow - 1)
        }
    }
}
