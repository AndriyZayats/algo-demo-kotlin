package mail.com.gmail.zayats.andriy.algo.cutting

import mail.com.gmail.zayats.andriy.algo.commons.IndexedInterval
import mail.com.gmail.zayats.andriy.algo.structures.buffer.ring.RingBuffer
import mail.com.gmail.zayats.andriy.algo.structures.list.prepending.Empty
import mail.com.gmail.zayats.andriy.algo.structures.list.prepending.PrependingList
import mail.com.gmail.zayats.andriy.algo.structures.list.prepending.prependTo
import java.lang.IllegalArgumentException

class Cutting(val totalPrice: Long, val slicesLength: Iterable<Int>)

/**
 * Calculate the best choice of slices to have the highest sum of slices prices
 * 
 * @param length max length of chosen slices
 * @param prices structure where index is slice length and value is slice price
 * 
 * Томас Кормен. Алгоритми (побудова та аналіз). 15.1. Розрізання стержня.
 */
fun bestCutting(length: Int, prices: IndexedInterval<Long>): Cutting {
    
    if (prices.indexOfFirst < 1) {
        throw IllegalArgumentException("prices indexes should be higher than 0")
    }

    fun appendToResult(previousResult: Cutting, sliceLength: Int, slicePrice: Long) =
        Cutting(
            previousResult.totalPrice + slicePrice,
            sliceLength prependTo previousResult.slicesLength as PrependingList<Int>
        )
    
    val emptyResult = Cutting(0, Empty)

    val results = RingBuffer<Cutting>(prices.indexOfLast)
    results[0] = emptyResult

    for (calculatedLength in 1..length) {
        results[calculatedLength] =
            prices.indexesInterval.asSequence()
                .filter {
                    sliceLength -> sliceLength <= calculatedLength
                }
                .map { sliceLength ->
                    appendToResult(
                        results[calculatedLength - sliceLength], 
                        sliceLength, prices[sliceLength]
                    )
                }
                .maxWith(CuttingResultsComparator) ?: emptyResult
    }

    return results[length]
}

private object CuttingResultsComparator : Comparator<Cutting> {

    override fun compare(o1: Cutting?, o2: Cutting?) =
        compareNotNullable(o1!!, o2!!)

    private fun compareNotNullable(o1: Cutting, o2: Cutting) =
        when {
            o1.totalPrice < o2.totalPrice -> -1
            o1.totalPrice > o2.totalPrice -> 1
            else -> 0
        }
}
