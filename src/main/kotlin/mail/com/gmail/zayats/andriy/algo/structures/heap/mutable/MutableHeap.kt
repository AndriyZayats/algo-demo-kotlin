package mail.com.gmail.zayats.andriy.algo.structures.heap.mutable

import java.lang.AssertionError
import java.util.NoSuchElementException

class MutableHeap<E> (private val firstDominatesSecond: (E, E) -> Boolean) {

    private val list = EndlessOneBaseIndexedList<E>()

    var size = 0
        private set

    constructor (firstDominatesSecond: (E, E) -> Boolean, elements: Collection<E>)
            : this(firstDominatesSecond) {

        elements.forEach {
            size++
            list[size] = it
        }
        heapifyAll()
    }

    fun getExtreme(): E =
        when {
            (size > 0) -> element(1)
            else -> throw NoSuchElementException("Heap structure is empty")
        }

    fun insert(value: E) {
        size++
        list[size] = value
        bubbleUp(size)
    }

    fun removeExtreme() {
        if (size == 0) {
            throw NoSuchElementException("Heap structure is empty")
        }
        // replace head with last element
        list[1] = element(size)
        size--
        if (size > 0) {
            heapify(1)
        }
    }

    private tailrec fun bubbleUp(i: Int) {
        if (hasParent(i)) {
            val element = element(i)
            val parentIndex = parentIndex(i)
            val parent = element(parentIndex)
            if (element dominates parent) {
                swap(i, parentIndex)
                bubbleUp(parentIndex)
            }
        }
    }

    private tailrec fun heapify(i: Int) {
        if (!hasLeft(i)) {
            return
        }
        val leftIndex = leftIndex(i)
        val left = element(leftIndex)

        val dominantChildIndex = if (hasRight(i)) {
            val rightIndex = rightIndex(i)
            val right = element(rightIndex)
            if (left dominates right) leftIndex else rightIndex
        } else {
            leftIndex
        }

        val dominantChild = element(dominantChildIndex)
        if (dominantChild dominates element(i)) {
            swap(i, dominantChildIndex)
            heapify(dominantChildIndex)
        }
    }

    private infix fun E.dominates(other: E) = firstDominatesSecond(this, other)

    private fun element(i: Int) = list[i]

    private fun hasParent(i: Int) = i > 1

    private fun hasLeft(i: Int) = leftIndex(i) <= size

    private fun hasRight(i: Int) = rightIndex(i) <= size

    private fun parentIndex(i: Int) = i / 2

    private fun leftIndex(i: Int) = i * 2

    private fun rightIndex(i: Int) = i * 2 + 1

    private fun swap(index1: Int, index2: Int) {
        val temp = list[index1]
        list[index1] = list[index2]
        list[index2] = temp
    }

    private fun heapifyAll() {
        for (i in (size / 2 + size % 2) downTo 1) {
            heapify(i)
        }
    }
}

private class EndlessOneBaseIndexedList<T> {

    private val list = mutableListOf<T>()

    operator fun get(i: Int) = list[i - 1]

    operator fun set(i: Int, value: T) {
        when {
            (list.size >= i) -> {
                list[i - 1] = value
            }
            
            (list.size == i - 1) -> {
                list.add(value)
            }
            
            else -> {
                throw AssertionError("Unexpected. Skipped index")
            }
        }
    }
}
