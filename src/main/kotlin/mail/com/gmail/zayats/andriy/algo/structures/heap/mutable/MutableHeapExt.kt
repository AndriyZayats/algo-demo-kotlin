package mail.com.gmail.zayats.andriy.algo.structures.heap.mutable

val MutableHeap<*>.isEmpty: Boolean
    get() = this.size == 0

fun <T> MutableHeap<T>.takeExtreme(): T {
    val extreme = this.getExtreme()
    this.removeExtreme()
    return extreme
}

fun <E> minHeap(comparator: Comparator<E>) = MutableHeap<E>(minDominatesFunction(comparator))

fun <E> maxHeap(comparator: Comparator<E>) = MutableHeap<E>(maxDominatesFunction(comparator))

private fun <E> minDominatesFunction(comparator: Comparator<E>) = // 
    { a: E, b: E -> comparator.compare(a, b) < 0 }

private fun <E> maxDominatesFunction(comparator: Comparator<E>) = //
    { a: E, b: E -> comparator.compare(a, b) > 0 }

fun <C : Comparable<C>> minHeap(vararg elements: C) = minHeap(elements.toList())

fun <C : Comparable<C>> maxHeap(vararg elements: C) = maxHeap(elements.toList())

fun <C : Comparable<C>> minHeap(elements: Collection<C>) =
    MutableHeap(minDominatesFunction(comparableComparator()), elements)

fun <C : Comparable<C>> maxHeap(elements: Collection<C>) =
    MutableHeap<C>(maxDominatesFunction(comparableComparator()), elements)

private fun <C : Comparable<C>> comparableComparator(): Comparator<C> = // 
    Comparator.comparing { element: C -> element }
