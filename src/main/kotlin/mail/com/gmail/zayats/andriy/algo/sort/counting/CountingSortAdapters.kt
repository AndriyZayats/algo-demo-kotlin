package mail.com.gmail.zayats.andriy.algo.sort.counting

fun byteArrayCountingSort(input: ByteArray): ByteArray = 
    countingSort(input.toTypedArray(), 256) {it.toInt() + 128}.toByteArray()
