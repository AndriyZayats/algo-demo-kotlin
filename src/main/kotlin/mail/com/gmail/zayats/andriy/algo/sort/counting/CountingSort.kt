package mail.com.gmail.zayats.andriy.algo.sort.counting

fun <T> countingSort(input: Array<T>, domainSize: Int, mapToOrdinal: (T) -> Int): Array<T> {
    val countPerOrdinal = IntArray(domainSize) {0}
    for (element in input) {
        val ordinal = mapToOrdinal(element)
        countPerOrdinal[ordinal]++
    }
    
    val resultedPositions = IntArray(domainSize)
    repeat(domainSize) { i ->
        resultedPositions[i] = 
            if (i == 0) 
                0 
            else 
                resultedPositions[i - 1] + countPerOrdinal[i - 1]
    }
    
    val result = input.copyOf()
    for (element in input) {
        val ordinal = mapToOrdinal(element)
        result[resultedPositions[ordinal]] = element
        resultedPositions[ordinal]++
    }
    
    return result
} 
