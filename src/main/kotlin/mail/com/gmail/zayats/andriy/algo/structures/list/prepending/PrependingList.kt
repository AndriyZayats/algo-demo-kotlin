package mail.com.gmail.zayats.andriy.algo.structures.list.prepending

import java.lang.IllegalStateException

sealed class PrependingList<out T> : Iterable<T> {
    
    override fun iterator(): Iterator<T> = PrependingListIterator(this)

    final override fun toString() = prependingListToString(this)
}

object Empty : PrependingList<Nothing>()

data class WithData<T>(

    val head: T,
    val tail: PrependingList<T>

) : PrependingList<T>()

fun <T> prependingListOf(vararg elements: T): PrependingList<T> {
    return elements.foldRight(Empty as PrependingList<T>) { element, acc ->
        element prependTo acc 
    }
}

infix fun <T> T.prependTo(list: PrependingList<T>): PrependingList<T> = WithData(this, list)

private class PrependingListIterator<T>(list: PrependingList<T>) : Iterator<T> {

    var current = list

    override fun hasNext() =
        when (current) {
            is Empty -> false
            is WithData -> true
        }

    override fun next(): T =
        current.let { currentValue ->
            when (currentValue) {
                is Empty -> throw IllegalStateException()
                is WithData -> currentValue.head
                    .also { current = currentValue.tail }
            }
        }
}

private fun prependingListToString(list: PrependingList<*>) =
    accumulatePrependingListToString(list, StringBuilder()).toString()

private tailrec fun accumulatePrependingListToString(list: PrependingList<*>, sb: StringBuilder) : StringBuilder =
    when (list) {
        is Empty ->
            when (sb.isEmpty()) {
                true -> sb.append("()")
                false -> sb.append(')')
            }
        is WithData ->
            when (sb.isEmpty()) {
                true -> accumulatePrependingListToString(list.tail, sb.append('(').append(list.head))
                false -> accumulatePrependingListToString(list.tail, sb.append(", ").append(list.head))
            }
    }
