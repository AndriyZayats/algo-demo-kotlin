package mail.com.gmail.zayats.andriy.algo.sequences.lcs

import mail.com.gmail.zayats.andriy.algo.commons.Indexed
import mail.com.gmail.zayats.andriy.algo.commons.toIndexed
import mail.com.gmail.zayats.andriy.algo.structures.buffer.ring.RingBuffer
import mail.com.gmail.zayats.andriy.algo.structures.list.prepending.Empty
import mail.com.gmail.zayats.andriy.algo.structures.list.prepending.PrependingList
import mail.com.gmail.zayats.andriy.algo.structures.list.prepending.prependTo
import mail.com.gmail.zayats.andriy.algo.structures.list.prepending.reverse

fun <T> longestCommonSubSequence(a: Indexed<T>, b: Indexed<T>): Indexed<T> {

    class Result(val length: Int, private val reversedSequence: PrependingList<T>) {

        val sequence
            get() = reversedSequence.reverse()

        infix fun add(element: T) = Result(this.length + 1, element prependTo this.reversedSequence)
    }
    
    fun betterOf(some: Result, other: Result) = 
        if (some.length > other.length)
            some
        else 
            other
    
    val emptyResult = Result(0, Empty)

    val (x, y) =
        if (a.size <= b.size) a to b
        else b to a

    val prefixesResultsTable = object {

        private val buffer =
            RingBuffer<Result>(x.size + 2)

        private fun index(row: Int, col: Int) =
            row * (x.size + 1) + col

        operator fun get(row: Int, col: Int) =
            buffer[index(row, col)]

        operator fun set(row: Int, col: Int, value: Result) {
            buffer[index(row, col)] = value
        }
    }

    repeat (x.size + 1) { i ->
        prefixesResultsTable[0, i] = emptyResult
    }

    for (ySubStr in 1..y.size) {
        prefixesResultsTable[ySubStr, 0] = emptyResult
        for (xSubStr in 1..x.size) {
            prefixesResultsTable[ySubStr, xSubStr] =
                if (x[xSubStr - 1] == y[ySubStr - 1]) {
                    prefixesResultsTable[ySubStr - 1, xSubStr - 1] add x[xSubStr - 1]
                } else {
                    betterOf(prefixesResultsTable[ySubStr, xSubStr - 1], prefixesResultsTable[ySubStr - 1, xSubStr])
                }
        }
    }

    return prefixesResultsTable[y.size, x.size].sequence.toList().toIndexed()
}
