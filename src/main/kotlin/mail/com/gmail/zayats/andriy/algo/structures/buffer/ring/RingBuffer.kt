package mail.com.gmail.zayats.andriy.algo.structures.buffer.ring

import java.lang.IllegalArgumentException
import kotlin.math.max

class RingBuffer<T>(val capacity: Int) {
    
    private val data = Array<Any?>(capacity) { null }
    
    var offset = 0
        get
        private set
    
    var size = 0
        get
        private set
    
    operator fun set(i: Int, value: T) {
        when {
            i < offset -> 
                throw IllegalArgumentException("buffer is over {i}")
            i > size ->
                throw IllegalArgumentException("buffer is behind {i}")
            i == size -> {
                size++
            }
        }
        offset = max(offset, size - capacity)
        data[i.rem(capacity)] = value
    }

    @Suppress("UNCHECKED_CAST")
    operator fun get(i: Int): T =
        when {
            i < offset ->
                throw IllegalArgumentException("buffer is over {i}")
            i >= size ->
                throw IllegalArgumentException("buffer is behind {i}")
            else ->
                data[i.rem(capacity)] as T
        }
}
