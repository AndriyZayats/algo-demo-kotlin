package mail.com.gmail.zayats.andriy.algo

import mail.com.gmail.zayats.andriy.algo.structures.list.prepending.Empty
import mail.com.gmail.zayats.andriy.algo.structures.list.prepending.PrependingList
import mail.com.gmail.zayats.andriy.algo.structures.list.prepending.prependTo
import mail.com.gmail.zayats.andriy.algo.structures.pairs.unordered.UnorderedPair

fun <N : Comparable<N>> findAllPairsWithSum(
    requiredSum: N, 
    elements: Iterable<N>, 
    addOperation: (N, N) -> N
): Set<UnorderedPair<N>> {
    
    infix fun N.add(other: N) = addOperation(this, other)

    val sortedDistinctList = elements.distinct().sorted()
    
    tailrec fun fold(
        startIndex: Int, 
        endIndex: Int, 
        aggregator: PrependingList<UnorderedPair<N>>
    ): Set<UnorderedPair<N>> {
        
        if (startIndex >= endIndex) {
            return aggregator.toSet()
        } else {
            val a = sortedDistinctList[startIndex]
            val b = sortedDistinctList[endIndex]
            val sum = a add b
            if (sum < requiredSum) {
                return fold(startIndex + 1, endIndex, aggregator)
            } else if (sum > requiredSum) {
                return fold(startIndex, endIndex - 1, aggregator)
            } else { // sum == requiredSum
                return fold(startIndex + 1, endIndex - 1, UnorderedPair(a, b) prependTo aggregator)
            }
        }
    }
    
    return fold(0, sortedDistinctList.size - 1, Empty)
}
